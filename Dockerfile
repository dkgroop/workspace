FROM php:5.6-fpm

# Переключаем Ubuntu в неинтерактивный режим — чтобы избежать лишних запросов
ENV DEBIAN_FRONTEND noninteractive 

RUN apt-get update && apt-get install -y \
                locales \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
		libpng-dev \
                libgearman-dev \
                libmcrypt-dev \
                libxml2-dev \
                uuid-dev

# Устанавливаем локаль
RUN echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen 

ENV LANG ru_RU.UTF-8  
ENV LANGUAGE ru_RU:ru  
ENV LC_ALL ru_RU.UTF-8

RUN  docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
	&& docker-php-ext-install -j$(nproc) exif \
	&& docker-php-ext-install -j$(nproc) gd \
	&& docker-php-ext-install -j$(nproc) intl \
	&& docker-php-ext-install -j$(nproc) dom \
	&& docker-php-ext-install -j$(nproc) soap \
	&& docker-php-ext-install -j$(nproc) mcrypt \
	&& docker-php-ext-install -j$(nproc) pcntl \
	&& docker-php-ext-install -j$(nproc) mysql \
	&& docker-php-ext-install -j$(nproc) pdo_mysql \
	&& docker-php-ext-install -j$(nproc) zip \
	&& docker-php-ext-install -j$(nproc) gd  \
        && pecl install redis-2.2.8 \
        && pecl install xdebug-2.5.5 \
        && pecl install gearman-1.1.2 \
        && pecl install uuid-1.0.4

RUN curl -fsSL 'https://xcache.lighttpd.net/pub/Releases/3.2.0/xcache-3.2.0.tar.gz' -o /tmp/xcache.tar.gz \
    && mkdir -p /tmp/xcache \
    && tar -xf /tmp/xcache.tar.gz -C /tmp/xcache --strip-components=1 \
    && cd /tmp/xcache \
    && phpize \
    && ./configure --enable-xcache \
    && make -j$(nproc) \
    && make install \
    && cd / \
    && rm -rf /tmp/xcache*

RUN docker-php-ext-enable uuid redis xdebug gearman xcache

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer config -g github-oauth.github.com b4e212f72779450b1db53ccd1e5d19d1e96f9569
RUN composer global require fxp/composer-asset-plugin  --no-interaction
RUN composer global require hirak/prestissimo

WORKDIR /var/app

